# INF226 Compulsory Assignment 2+3 (Fall 2022)
## Practical information

* **Deadline**
  * **Friday, November 4th, 2022 at 12:00** (Part 2A and 2B)
  * **Friday, November 11th, 2022 at 12:00** (Part 3)

* This assignment counts towards your grade (20% + 10%). (The minimum required to pass is *0 points*, since we're running late and very close to the exam.)
* You may complete this assignment in groups of 1–3 students (with more students, we expect more results, though!) See general UiB guidelines on [cheating and its consequences](https://www.uib.no/en/quality-in-studies/77940/cheating-and-its-consequences) and [using sources](https://www.uib.no/en/education/49058/use-sources-written-work)
* The goal is to finish a prototype web application and reflect on its security.

### Objectives
Afterwards, you should be able to explain…

* what the main threats against web services are,
* and how to mitigate them;

and you should be able to…

* work with sessions and cookies,
* use libraries for authentication,
* do simple security analysis of an application

Also, you should have a rough idea of browser security features like the [same-origin policy](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy), and maybe also [cross-origin resource sharing](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

### Deliverables

Deliver your work as a *link to a Git repository*, with:

* a `README.md` file with a brief overview and answers to questions
* the source code of your project

## Part 2A – implementation

* Start with the [login server project](https://git.app.uib.no/inf226/22h/login-server) from the previous exercise
* The code is badly structured (everything mixed in one file):
   * Make a note of what's wrong with the structure (and anything else you see that may be problematic) – could any of these things have a security impact?
   * Refactor and improve the code – see the *Secure by Design* book for tips, document your choices in `README.md`
* Use a database to persist information. You can continue with the SQLite database already included or use another one. (Redis is a popular non-relational choice.) Note that there are several kinds of information involved that might be stored in different ways:
   * *session data* – e.g., who's logged in – handled by Flask (or whatever framework you're using), you just need to [configure it](https://flask.palletsprojects.com/en/2.2.x/api/#sessions)
   * *auth data* – e.g, user database with passwords – could possibly be stored separately 
   * *application data* – for a messaging app, that would be the actual messages + public profile info for users, etc
* Implement a simple instant messaging system, for example, a *message* has:
   * an *ID*,
   * a *sender* and one or more *recipients* (all *users* in the user database),
   * some *content* (plain text / Markdown / HTML),
   * a *timestamp* and perhaps other metadata (delivered, seen, etc),
   * for a reply, the ID of the message being replied to
* You need a simple messaging API:
   * `POST /new` to create a new message 
   * `GET /messages` to retrieve (a list of) all messages
   * `GET /messages/ID` to retrieve a single message
* Authentication – [implement login](https://git.app.uib.no/inf226/22h/login-server/-/blob/master/README.md)
* Authorization – each user should only be able to see his/her own messages
   * Optionally, you might make it possible to block users or have some other restriction on sending messages

You are free to decide on details yourself, and do more advanced stuff (see below for tips).
#### Particular things to consider
Is your application vulnerable to

* [Cross-site scripting attacks](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)?
* [Cross-site request forgery](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html)?
* [SQL injection](https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html)?
* [Insecure design](https://owasp.org/Top10/A04_2021-Insecure_Design/)?
* 
#### Resources
* *Security for Software Engineers* (textbook, available online through UiB's network) – Ch. 5–9 on code hardening, ch. 10 & 11 on authentication and access control
* *Secure by Design* (textbook)
* *[OWASP Web Service Security Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Web_Service_Security_Cheat_Sheet.html)*
* 
## Part 2B – Documentation

* Write a `README.md` with:
   * a brief overview of your design considerations from Part A,
   * the features of your application,
   * instructions on how to test/demo it, 
   * technical details on the implementation,
   * answers to the questions below

#### Questions
* [Threat model](https://cheatsheetseries.owasp.org/cheatsheets/Threat_Modeling_Cheat_Sheet.html) – who might attack the application? What can an attacker do? What damage could be done (in terms of *confidentiality*, *integrity*, *availability*)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?
* What are the main attack vectors for the application?
* What should we do (or what have you done) to protect against attacks?
* What is the [access control model](https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html)?
* How can you know that you security is good enough? ([*traceability*](https://cheatsheetseries.owasp.org/cheatsheets/Logging_Cheat_Sheet.html)) 

You'll probably find the *Security for Software Engineers* textbook helpful here, as well as the [OWASP Attack Surface Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Attack_Surface_Analysis_Cheat_Sheet.html)

## Part 3 – Review

For this part, you'll get access to another project. Examine the documentation and the code, and try running the application. Write a short security analysis –

* Do you understand the overall design, based on the documentation and available source code?
* Do you see any potential improvements that could be made in design / coding style / best practices? I.e., to make the source code clearer and make it easier to spot bugs.
* Based on the threat model described by the author(s) (from part 2B), do you see any vulnerabilities in the application?
* Do you see any security risks that the author(s) *haven't* considered? (Have a look at the [OWASP Top Ten list](https://owasp.org/www-project-top-ten/) and  “Unit 2: Attack Vectors” in *Security for Software Engineers*)
* 

You may find it useful to try [some security testing tools](https://hackr.io/blog/top-10-open-source-security-testing-tools-for-web-applications). Also, think back to the techniques you used in the exercise on [XSS attacks](https://xss-game.appspot.com/) – what sort of damage could you do by tricking a logged-in user into click on a link?

## Basic Tips (for 2A)

## Advanced Tips (for 2A)

## Boilerplate
This is the same stuff as in Assignment 1:

### Plagiarism / Cheating / Collaboration

* The report should be fully written by you. Any code / text / quotes taken from anywhere else (including your own previous work) must be clearly marked with the source (for example, using BibTeX references or footnotes, and literal quotes shown with different formatting).
* Any tool/library you use must be mentioned, and if you follow any online guide/tutorial or similarly, you should refer to it in the report.
* You may discuss / sit together with / try and figure things out together with other students, but the written report must be *your own work*. 
* When/if helping each other, please try to not ask for / give *solutions*, but rather explanations of how things work, or other hint. Explaining how [you think] your [non-functioning] code works can be useful for both you and the [your collaborator](https://rubberduckdebugging.com/).
* If possible, try to [ask for help on Discord](https://discord.gg/he3PN52uJG) – there are probably others who wonder about the exact same thing, but are too shy to ask!
* But: *don't* post screenshots/listings with your (almost) complete solution, or showing the flag, etc. There are still lots of useful things to ask/answer regarding library function, how does return-oriented-programing work, etc.

### Deadline extensions, etc

If you for some reason see that you can't deliver on time, let your TA know as soon as possible. You don't have to explain *why* – we trust that you have a sensible reason (and understand if you don't).

* You can get an (up to) 48 hour extension if you need it. 
* If you *really* need a longer extension, please include a draft of whatever you have so far (even if it's nothing) and a brief plan for how you intend to complete the exercise in the remaining time.

* If you've solved the assignment and just think you could do a bit better with a bit more time – don't bother, you probably have more useful stuff to do, and small fixes won't make any practical difference.
