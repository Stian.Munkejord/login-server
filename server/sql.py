import apsw
from apsw import Error
import sys
from server import password
import base64
import app
from json import dumps

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()

except Error as e:
    print(e)
    sys.exit(1)


def getUsers() -> list:
    stmt = f"SELECT username, password FROM Users"
    res = conn.execute(stmt)
    out = {}
    for row in res:
        out[row[0]] = row[1]
    return out


def getUser(username):
    stmt = f"SELECT username, password FROM Users WHERE username ='{username}'"
    res = conn.execute(stmt)
    out = {}
    for row in res:
        out[row[0]] = row[1]
    return out


def userInDB(username) -> bool:
    users = getUsers()
    if username in users:
        return True
    return False


def insertUser(user: str, Pword: str):
    pword = password.hashing(Pword.encode("utf-8")).decode("utf-8")
    # username = password.hashing(user.encode("utf-8")).decode("utf-8")
    # stmt = f"INSERT INTO Users VALUES (NULL,'{user}','{pword}')"
    if not userInDB(user):
        cursor = conn.execute('INSERT INTO Users (username,password) VALUES (?,?)', (user, pword))


def dmMessages(first_person:str, secound_person:str):
    stmt = f"SELECT * FROM messages WHERE sender = '{first_person}' AND reciver = '{secound_person}' UNION SELECT * FROM messages WHERE sender = '{secound_person}' AND reciver = '{first_person}' ORDER BY id"
    result = f"Query: {app.pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)


def allmsg():
    stmt = f"SELECT * FROM messages WHERE ((sender OR reciver) = '{User.username}') or (reciver = 'Everyone')"
    result = f"Query: {app.pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)
