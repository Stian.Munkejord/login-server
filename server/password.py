import bcrypt
import base64


def hashing(string):
    hashed = bcrypt.hashpw(string, bcrypt.gensalt())
    return hashed


def checkPword(password, hashedP):
    print(password)
    print(hashedP)
    return bcrypt.checkpw(password.encode("utf-8"), hashedP.encode("utf-8"))
