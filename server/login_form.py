from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    checkbox = BooleanField()
    submit = SubmitField('Submit')


class SignupForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

