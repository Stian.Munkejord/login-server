import flask_login
from flask import Flask, abort, request, send_from_directory, make_response, render_template, g
import flask

from server.login_form import LoginForm, SignupForm

from json import dumps
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token
from threading import local
from markupsafe import escape

from is_safe_url import is_safe_url
from server import userManager, sql, password

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
app.secret_key = '9bc9d4bd6780a13e652059cfe58dcf3a5198ae4a7f83389af40bb1ffa26cb772'
userManager.login_manager.init_app(app)


def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap=True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'


@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'templates/favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'templates/favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@userManager.login_required
def index_html():
    return send_from_directory(app.root_path,
                               'templates/index.html', mimetype='text/html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        pword = form.password.data
        remember_user = form.checkbox.data

        u = userManager.user_attempted_login(username, pword)

        if u:
            user = userManager.user_loader(username)

            userManager.login_user(user, remember=remember_user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')

            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index'))
    return render_template('./login.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)

    if form.validate_on_submit():
        username = form.username.data
        pword = form.password.data

        if not sql.userInDB(username):
            sql.insertUser(username, pword)

            user = userManager.user_loader(username)
            userManager.login_user(user, True)

            flask.flash('Signed up and logged in successfully.')

            next = flask.request.args.get('next')

            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))

    return render_template('./signup.html', form=form)


@app.get('/search')
@userManager.login_required
def search():
    current_username = flask_login.current_user.id
    requested_username = request.args.get('q')

    try:
        return sql.dmMessages(current_username, requested_username)
    except Error as e:
        return (f'{"error"}ERROR: {e}', 500)


@app.route('/send', methods=['POST', 'GET'])
def send():
    try:
        sender = flask_login.current_user.id
        message = request.args.get('message') or request.form.get('message')
        reciver = request.args.get('reciver') or request.form.get('reciver')
        if not sender or not message or not reciver:
            return f'ERROR: missing sender or message'

        stmt = f"INSERT INTO messages (sender, reciver, message) values (?,?,?);"

        result = f"Query: {pygmentize(stmt)}\n"
        conn.execute(stmt, (sender, reciver, message,))
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'


@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender': escape(row[0]), 'message': escape(row[1])})
        return {'data': anns}
    except Error as e:
        return {'error': f'{e}'}


@app.route("/logout")
@userManager.login_required
def logout():
    userManager.logout_user()
    return flask.redirect("/")


@app.get('/coffee/')
def nocoffee():
    abort(418)


@app.route('/coffee/', methods=['POST', 'PUT'])
def gotcoffee():
    return "Thanks!"


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp


try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        reciver TEXT NOT NULL,
        message TEXT NOT NULL,
        sendt DATETIME DEFULT CURRENT_TIMESTAMP);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS Users (
        user_id INTEGER PRIMARY KEY AUTOINCREMENT,
        username VARCHAR NOT NULL, 
        password VARCHAR NOT NULL);''')
    c.execute('''CREATE TRIGGER insert_Timestamp_Trigger
                AFTER INSERT ON messages
                BEGIN
                UPDATE messages SET sendt =STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW') WHERE id = NEW.id;
                END;

                CREATE TRIGGER update_Timestamp_Trigger
                AFTER UPDATE On messages
                BEGIN
                UPDATE messages SET sendt = STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW') WHERE id = NEW.id;
                END;''')
    sql.insertUser('bob', 'bananas')
    sql.insertUser("alice", "password123")
except Error as e:
    print(e)
    sys.exit(1)
