
## Part 2B – Documentation

### Brief overview of design considerations from Part A:
What's problematic with the original code:
* The search and send forms are really exposed to SQL-injection. 
* The secret key should be something random and unique.
* Having everything in app.py is horrible coding practice. It can lead to 
confusion about what different methods and classes actually do, and code in one part of the file
relying on some code far away in the file, without a clear reason. It can also lead to there
being a massive amount of imports in one single file, which can make it hard to update the 
code if some import becomes obsolete or vulnerable. app.py should be split into different files with clear 
responsibilities: e.g. a file for managing some of the sql, and one for managing the users. 


### Features of our application: 
Sign-up page:<br>
    A new user is created if one does not already exist. <br>

Login-page:<br>
    Input a username and password. A valid username and related password gives access to the site. <br>

Chat:<br>
To send a message: <br>
    Write the username of the recipient in the "To:"-field.<br>
    Write your message in the "Message:"-field. <br>
    Press send. Your logged in username is automatically set as the sender. You can ignore the "from" field. <br><br>

To view all messages between you and another user: <br>
    Write the username of the user in the "search:"-field. <br>
    Press "Search!".

Logout:
    User is logged out and brought back to the login page.


### Instructions on how to test/demo: 
Install all required packages and modules <br>
Open the project in your preferred editor <br>
Write "flask run" in your terminal <br>




### Technical details on implementation: 
Creating a new username and password stores the user in the database. Passwords are hashed and salted. 
Inputs to the database are sanitized through prepared statements. 


## Questions

### Threat model

<b>Who might attack the application? </b> <br>
As our site is used to chat and communicate, the potential attackers include
anyone with an interest in accessing or interrupting the communication between our users. 
Depending on our userbase, this can include hacker groups with a goal of financial gain through
extortion, state actors gathering intelligence, and private persons looking for a challenge. 

<b>What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? </b> <br>
Any outside entity accessing the communication of two users' communication would be a breach of the confidentiality. 
A breach of the integrity would be an outside entity being able to appear as and distribute messages as another
user, or being able to alter the database and alter the sender, recipient, content or metadata of previous messages. 
An attacker could send our server lots of useless requests to disturb the availability of our server to other users. 

<b>Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against? </b> <br>
There are some limits to what an attacker can do, but for a service as small as ours they can do quite a lot. 
If they were to get full access to our databases and server code, they would as mentioned earlier be able to 
portray themselves as someone they were not, but only within our site. The maximum level of damage an attacker
could inflict would probably be to sell all metadata, personal information and communication related to the users.
Selling usernames and password databases can also occur, but this is less dangerous than before. Even if we
assumed that any user used the same password for all their authentication, most modern and crucial websites require 
some form of Two-Factor Authentication, so a password leak alone would probably not give an attacker direct access
to a users' logins in separate systems. We also of course hash and salt the passwords of the users. 

There are limits to what we can sensibly protect against. Software security is always a sum of cost, use and functionality.
You could keep constant surveillance of everyone working on the project, to make sure they haven't been paid off
by an attacker to perform some malicious action, which would be great security wise, but pretty horrible for the cost.
Instead of doing this we make sure that every user, even developers, only have access to that which they should have
access to, and we make sure that we have proper authentication to avoid one developer borrowing another developers' computer
to sneak past the access control. 


<b>What are the main attack vectors for the application?</b><br>
The main attack vectors for this application are some usual ones: Broken Access Control, Software And Data 
Integrity Failure, and Security Logging and Monitoring Failures. Our access control consists of checking if 
a user is the sender and recipient of a message, to decide whether a message should be displayed. The security
being a direct part of the development and software is a good thing, but the application still lacks dedicated access control. 
The application also lacks proper segregation of, configuration and access control to ensure the integrity of the 
code flowing through the build and deploy processes. As for the Security Logging and Monitoring Failures there
should be more logging of user actions, requests, output, and session information like IP addresses. 


<b>What should we do (or what have you done) to protect against attacks?</b> <br>
Our main upgrade against attacks is sanitising inputs, mainly by using prepared statements, to protect against SQL-injection.
Using is_safe_url ensures that a redirect target leads to the same server, as URLs can be abused in cross-site scripting
attacks.

We have split app.py into multiple, more clear files, to avoid developers losing track of what something does. 

Smaller protections: 
* changed the secret key to a cryptographically strong value generated using python secrets, instead of 
the default pseudo-random generators that usually are more designed for modelling and simulation. 
* "remember me for 31 days"-checkbox, which adds some convince for the user when logging on from a home device,
and allows them to have a shorter login session when on more public devices. 


<b>What is the access control model?</b><br>
Access control models decide what users can access certain objects or perform various operations, 
for example in the form of reading/writing to files and database tables, and starting processes or allocate memory. 
It also brings the necessity of controlling which users can grant/revoke access in the system. 

When it comes to [<b>the</b> access control model](https://learn.microsoft.com/en-us/windows/win32/secauthz/access-control-components)
we have two basic parts: access tokens and security descriptors. 

A security descriptor contains security information about an object, specified by its creator, or just default security information. 
It identifies the objects' owner, and can also contain a discretionary access control list, and a system access control list. 
The discretionary access control list identifies the users and groups allowed or denied access to the object. 
The system access control list controls how the system audits attempts to access the object. 

An access token is created when a user has been authenticated by the system (e.g. through a username and password). 
It contains information about the user's account, group accounts to which the user belongs, a list of the privileges held
by the user or user's group, and another information. This token is used by the system, when a process tries to access a 
securable object (an object that can have a security descriptor) or attempts to perform other administration tasks that require
privileges, to identify the user that has called the process. 

<b>Edit close to deadline:</b><br>
Someone has pointed out that the question probably asks for the access control model of the app, not a general
explanation of access control models. As mentioned earlier in the answers, we don't have dedicated access control,
but "Our access control consists of checking if a user is the sender and recipient of a message, to decide whether a message 
should be displayed.". This is closest to a Discretionary Access Control (DAC). The "subjects" (users) can determine who
has access to their "objects" (messages) by choosing a receiver. 


<b>How can you know that your security is good enough? (traceability)</b> <br>
When implementing a "secure" system, the best way to have a secure system is to have as little as possible attack 
surface on the application. However, since there is no such thing as a secure system, 
traceability is important. This is because if your application would become compromised, you need to be able to
trace where the breach occurred, and what the attacker did.
